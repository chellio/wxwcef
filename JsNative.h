#ifndef JSNATIVE_H
#define JSNATIVE_H

#include <include/cef_app.h>

class JsNative : public CefV8Handler
{
    public:
        JsNative();

        bool Execute(const CefString& name, CefRefPtr<CefV8Value>object, const CefV8ValueList& arguments, CefRefPtr<CefV8Value>& retval, CefString& exception);

    protected:

    private:

        IMPLEMENT_REFCOUNTING(JsNative);

};

#endif // JSNATIVE_H
