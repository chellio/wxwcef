#ifndef RESPONSEFILTER_H
#define RESPONSEFILTER_H

#include <include/cef_response_filter.h>
#include <wx/file.h>

/* class used to save swf files */

class ResponseFilter : public CefResponseFilter
{
    public:
        ResponseFilter(CefString fullname_);

        bool InitFilter();
        CefResponseFilter::FilterStatus Filter(void* data_in, size_t data_in_size, size_t& data_in_read, void* data_out, size_t data_out_size, size_t& data_out_written);

    protected:

    private:
        CefString fullname;
        wxFile file;
        IMPLEMENT_REFCOUNTING(ResponseFilter);
};

#endif // RESPONSEFILTER_H
