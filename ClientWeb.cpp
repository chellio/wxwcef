
#include "ClientWeb.h"
#include "wxwMainFrame.h"
#include "wxwCefPanel.h"
#include "ResponseFilter.h"

#include <wx/msgdlg.h>
#include <wx/uri.h>
#include <wx/filename.h>
#include <wx/app.h>

ClientWeb::ClientWeb(wxwCefPanel *cef_panel = NULL)
{
    m_cef_panel = cef_panel;
    //ctor
}

void ClientWeb::OnAfterCreated(CefRefPtr<CefBrowser>browser)
{
}

bool ClientWeb::DoClose(CefRefPtr<CefBrowser>browser)
{
    return false;
}

void ClientWeb::OnBeforeClose(CefRefPtr<CefBrowser>browser)
{

}


/* CefClient */
bool ClientWeb::OnProcessMessageReceived(CefRefPtr<CefBrowser>browser, CefProcessId source_process, CefRefPtr<CefProcessMessage>message)
{
    LOG(INFO)<<"Return message from: "<<message->GetName().ToString();
    wxString str = message->GetArgumentList()->GetString(0).ToString();

    wxwMainFrame *mf = dynamic_cast<wxwMainFrame*>(m_cef_panel->GetParent());

    (mf->*msgback_handlers[message->GetName()])(str);

    return true;
}


/* CefRequestHandler */
CefRefPtr<CefResourceHandler> ClientWeb::GetResourceHandler(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame,
                                                                CefRefPtr<CefRequest>request)
{
    // Injecting scripts
    wxURI uri(request->GetURL().ToString());
    if( uri.GetServer() == "client.xyz" && uri.GetScheme() == "https")
    {
        return new ResourceHandler;
    }

    return NULL;
}

CefRefPtr<CefResponseFilter> ClientWeb::GetResourceResponseFilter(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame, CefRefPtr<CefRequest>request, CefRefPtr<CefResponse>response)
{
    // save swf files
    wxURI uri(request->GetURL().ToString());
    wxFileName fn = uri.GetPath();

    if(fn.GetExt() == "swf")
    {
        LOG(INFO)<<fn.GetFullPath();
        wxFileName dir("swf/");
        if(!dir.DirExists())
        {
            wxFileName::Mkdir(dir.GetPath());
        }

        return new ResponseFilter(fn.GetFullName().ToStdString());

    }

    return NULL;
}


/* CefLoadHandler */

void ClientWeb::OnLoadStart(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame)
{
}

void ClientWeb::OnLoadEnd(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame, int httpStatusCode)
{
}


/* CefFocusHandler */

void ClientWeb::OnGotFocus(CefRefPtr<CefBrowser>browser)
{
    m_cef_panel->setFocusExplicitly();
}


