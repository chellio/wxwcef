#ifndef APPHANDLER_H
#define APPHANDLER_H

#include <include/cef_app.h>

class AppHandler : public CefApp, public CefBrowserProcessHandler, public CefRenderProcessHandler
{
    public:

        AppHandler();

        CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() { return this; }
        CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() { return this; }

        void OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar>registrar);
        void OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine>command_line);

        /* Browser Process Handler */
        void OnContextInitialized();
        void OnRenderProcessThreadCreated(CefRefPtr<CefListValue>extra_info); // START IPC

        /* Render Process Handler */
        void OnContextCreated(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame, CefRefPtr<CefV8Context>context);
        bool OnProcessMessageReceived(CefRefPtr<CefBrowser>browser, CefProcessId source_process, CefRefPtr<CefProcessMessage>message);
        void OnBrowserCreated(CefRefPtr<CefBrowser>browser);
        void OnRenderThreadCreated(CefRefPtr<CefListValue>extra_info); // START IPC

        CefRefPtr<CefBrowser> m_browser;
    protected:

    private:

        IMPLEMENT_REFCOUNTING(AppHandler);
};



#endif // APPHANDLER_H
