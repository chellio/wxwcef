
#include <X11/Xlib.h>
#include <include/cef_app.h>
#include <include/cef_client.h>

#include <wx/wx.h>

#include "AppHandler.h"
#include "wxwApp.h"
#include <iostream>

namespace {

int XErrorHandlerImpl(Display *display, XErrorEvent *event) {
  LOG(WARNING)
        << "X error received: "
        << "type " << event->type << ", "
        << "serial " << event->serial << ", "
        << "error_code " << static_cast<int>(event->error_code) << ", "
        << "request_code " << static_cast<int>(event->request_code) << ", "
        << "minor_code " << static_cast<int>(event->minor_code);
  return 0;
}

int XIOErrorHandlerImpl(Display *display) {
  return 0;
}

}  // namespace

int main(int argc, char *argv[] )
{

    CefMainArgs args(argc, argv);

    CefRefPtr<AppHandler> app_handler  = new AppHandler;

    int ret = CefExecuteProcess(args, app_handler, NULL);
    if(ret>=0)
        return ret;

    XSetErrorHandler(XErrorHandlerImpl);
    XSetIOErrorHandler(XIOErrorHandlerImpl);


    CefSettings sett;
    sett.remote_debugging_port = 2600;
    sett.multi_threaded_message_loop = false;

    std::string cache_path = std::string(wxGetCwd() + "/cache");
    CefString(&sett.cache_path).FromString(cache_path);

    CefInitialize(args, sett, app_handler, NULL );

    wxwApp *app = new wxwApp;

    wxEntry(argc, argv);

    CefShutdown();


}
