#include "ResourceHandler.h"

#include <wx/filename.h>
#include <wx/uri.h>

bool ResourceHandler::ProcessRequest(CefRefPtr<CefRequest>request, CefRefPtr<CefCallback>callback)
{
    LOG(INFO)<<"Handlig resource with url "<<request->GetURL().ToString();

    wxURI uri(request->GetURL().ToString());

    if(!wxDirExists("scripts"))
    {
        wxMkdir("scripts");
    }

    wxString file_path = wxGetCwd() + uri.GetPath();

    if( wxFileName::FileExists( file_path) )
    {
        file.Open(file_path);
    }
    if( !file.IsOpened() )
    {
        LOG(INFO)<<file_path<<" failed to open";
        return false;
    }

    LOG(INFO)<<file_path<<" opened";

    length = file.Length();

    callback->Continue();

    return true;
}

void ResourceHandler::GetResponseHeaders(CefRefPtr<CefResponse> response, int64& response_length, CefString& redirectUrl)
{
    response_length = length;
    response->SetMimeType("application/javascript");
    response->SetStatus(200);
}

bool ResourceHandler::ReadResponse(void* data_out, int bytes_to_read, int& bytes_read, CefRefPtr<CefCallback>callback)
{
    if(!file.Eof())
    {
        bytes_read = file.Read(data_out, bytes_to_read);
        return true;
    }

    file.Close();
    return true;
}

void ResourceHandler::Cancel()
{
}


