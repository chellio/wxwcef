#include "wxwCefPanel.h"

//(*InternalHeaders(wxwCefPanel)
#include <wx/string.h>
#include <wx/intl.h>
//*)

#include<gtk/gtk.h>
#include<gdk/gdkx.h>
#include<wx/msgdlg.h>
#include<json.hpp>

#include "ClientWeb.h"

//(*IdInit(wxwCefPanel)
//*)

BEGIN_EVENT_TABLE(wxwCefPanel,wxPanel)
	//(*EventTable(wxwCefPanel)
	//*)
	EVT_SIZE(wxwCefPanel::OnSize)
END_EVENT_TABLE()

wxwCefPanel::wxwCefPanel(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size, long, const wxString &)
{
	//(*Initialize(wxwCefPanel)
	Create(parent, id, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("id"));
	//*)
}

wxwCefPanel::~wxwCefPanel()
{
	//(*Destroy(wxwCefPanel)
	//*)
}

void wxwCefPanel::OnSize(wxSizeEvent& evt)
{
    if(!m_browser)
        return;

    CefWindowHandle xid_window = m_browser->GetHost()->GetWindowHandle();
    XResizeWindow(cef_get_xdisplay(), xid_window, evt.GetSize().GetWidth(), evt.GetSize().GetHeight() );

}

// To remove caret from text entry widgets, when browser is activated
void wxwCefPanel::setFocusExplicitly()
{
    LOG(INFO)<<"Focus received ";
    this->SetFocus();
}

void wxwCefPanel::embedCefBrowser()
{
    m_client_web = new ClientWeb(this);

    CefBrowserSettings sett;
    CefWindowInfo wndinfo;

    CefWindowHandle x_handle = gdk_x11_window_get_xid( gtk_widget_get_window( this->GetHandle() ));
    CefRect rect(0,0,this->GetClientSize().GetWidth(), this->GetClientSize().GetHeight());
    wndinfo.SetAsChild(x_handle, rect );

    m_browser = CefBrowserHost::CreateBrowserSync(wndinfo, m_client_web, "", sett, NULL);
    m_browser->GetMainFrame()->LoadURL("http://google.pl/");

}

void wxwCefPanel::sendJSON(CefString function_name, json &j, void (wxwMainFrame::*ptr)(wxString))
{

    m_client_web->msgback_handlers[function_name] = ptr;

    CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create(function_name);
    CefRefPtr<CefListValue> args = msg->GetArgumentList();
    args->SetString(0, j.dump());
    m_browser->SendProcessMessage(PID_RENDERER, msg);
}



