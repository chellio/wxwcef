#include "ResponseFilter.h"
#include <wx/filename.h>


ResponseFilter::ResponseFilter(CefString fullname_)
{
    fullname = fullname_;
    file.Create("swf/" + fullname.ToString(), true);
}

bool ResponseFilter::InitFilter()
{
    return true;
}

CefResponseFilter::FilterStatus ResponseFilter::Filter(void* data_in, size_t data_in_size, size_t& data_in_read, void* data_out, size_t data_out_size, size_t& data_out_written)
{
    LOG(INFO)<<data_in_size<<" "<<data_out_size;

    data_in_read = data_in_size;

    data_out_written = std::min(data_in_read, data_out_size);

    memcpy(data_out, data_in, data_out_written );

    file.Write(data_in, data_out_written);

    return RESPONSE_FILTER_DONE;

}
