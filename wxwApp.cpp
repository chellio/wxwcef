/***************************************************************
 * Name:      wxwApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2016-06-26
 * Copyright:  ()
 * License:
 **************************************************************/

#include "wxwApp.h"

//(*AppHeaders
#include "wxwMainFrame.h"
#include <wx/image.h>
//*)


bool wxwApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	wxwMainFrame* Frame = new wxwMainFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)

    dynamic_cast<wxwMainFrame*>(GetTopWindow())->afterCreated();

    timer = new rotatorTimer;
    timer->Start(11);

    return wxsOK;

}

int wxwApp::OnExit()
{
    timer->Stop();
    delete timer;

    return 0;
}

void rotatorTimer::Notify()
{
    CefDoMessageLoopWork();
}

