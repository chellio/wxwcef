/***************************************************************
 * Name:      rotatorApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2016-06-26
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef ROTATORAPP_H
#define ROTATORAPP_H

#include <wx/app.h>
#include <wx/timer.h>

class rotatorTimer;

class wxwApp : public wxApp
{
    public:
        virtual bool OnInit();
        int OnExit();

    private:
        rotatorTimer *timer;
};


class rotatorTimer : public wxTimer
{
private:
    void Notify();
};

#endif // ROTATORAPP_H

