#ifndef SCHEMEHANDLER_H
#define SCHEMEHANDLER_H

#include<include/cef_app.h>
#include<include/cef_urlrequest.h>
#include<wx/string.h>
#include<wx/file.h>

class ResourceHandler : public CefResourceHandler
{
    friend class RequestClient;

public:

// ResourceHandler() : offset_(0) {}

    bool ProcessRequest(CefRefPtr<CefRequest>request, CefRefPtr<CefCallback>callback);
    void GetResponseHeaders(CefRefPtr<CefResponse>response, int64& response_length, CefString& redirectUrl);
    bool ReadResponse(void* data_out, int bytes_to_read, int& bytes_read, CefRefPtr<CefCallback>callback);
    void Cancel();

    private:
        wxFile file;
        wxFileOffset length;

    IMPLEMENT_REFCOUNTING(ResourceHandler);
};

class RequestClient : public CefURLRequestClient
{
public:

    RequestClient(CefRefPtr<ResourceHandler> rh) : data_owner(rh) {}

    void OnDownloadData(CefRefPtr<CefURLRequest>request, const void* data, size_t data_length);
    void OnRequestComplete(CefRefPtr<CefURLRequest>request);

    void OnUploadProgress(CefRefPtr<CefURLRequest>request, int64 current, int64 total) {}
    void OnDownloadProgress(CefRefPtr<CefURLRequest>request, int64 current, int64 total) {}
    bool GetAuthCredentials(bool isProxy, const CefString& host, int port, const CefString& realm,
                             const CefString& scheme, CefRefPtr<CefAuthCallback>callback) {}
private:
    CefRefPtr<ResourceHandler> data_owner;

    IMPLEMENT_REFCOUNTING(RequestClient);
};


#endif // SCHEMEHANDLER_H
