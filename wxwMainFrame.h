#ifndef WXWMAINFRAME_H
#define WXWMAINFRAME_H



//(*Headers(wxwMainFrame)
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/panel.h>
#include <wx/frame.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

#include <json.hpp>
using json = nlohmann::json;

#include "ClientWeb.h"

class wxwCefPanel;

class wxwMainFrame: public wxFrame
{
	public:

		wxwMainFrame(wxWindow* parent);
		virtual ~wxwMainFrame();

        void afterCreated();

        bool js_busy = false;

        void msgback_handler1(wxString str);

		//(*Declarations(wxwMainFrame)
		wxPanel* Panel1;
		wxwCefPanel* pn_web;
		wxButton* Button_1;
		wxTextCtrl* TextCtrl1;
		wxStaticText* StaticText2;
		//*)

	protected:

		//(*Identifiers(wxwMainFrame)
		static const long ID_STATICTEXT1;
		static const long ID_TEXTCTRL2;
		static const long ID_BUTTON2;
		static const long ID_PANEL2;
		static const long ID_PANEL1;
		//*)

	private:

		//(*Handlers(wxwMainFrame)
		void OnButton_1Click(wxCommandEvent& event);
		//*)

        void OnGetFocus(wxFocusEvent &evt);


        CefRefPtr<CefListValue> m_msg;

		DECLARE_EVENT_TABLE()
};

#endif
