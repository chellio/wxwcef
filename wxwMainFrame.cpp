#include "wxwMainFrame.h"

//(*InternalHeaders(wxwMainFrame)
#include <wx/string.h>
#include <wx/intl.h>
//*)

#include <gtk/gtk.h>

#include <wx/msgdlg.h>

#include "wxwCefPanel.h"

//

//(*IdInit(wxwMainFrame)
const long wxwMainFrame::ID_STATICTEXT1 = wxNewId();
const long wxwMainFrame::ID_TEXTCTRL2 = wxNewId();
const long wxwMainFrame::ID_BUTTON2 = wxNewId();
const long wxwMainFrame::ID_PANEL2 = wxNewId();
const long wxwMainFrame::ID_PANEL1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(wxwMainFrame,wxFrame)
	//(*EventTable(wxwMainFrame)
	//*)
END_EVENT_TABLE()

wxwMainFrame::wxwMainFrame(wxWindow* parent)
{

	//(*Initialize(wxwMainFrame)
	wxBoxSizer* BoxSizer2;
	wxBoxSizer* BoxSizer1;

	Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
	SetClientSize(wxSize(887,450));
	BoxSizer1 = new wxBoxSizer(wxVERTICAL);
	Panel1 = new wxPanel(this, ID_PANEL2, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL2"));
	BoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
	BoxSizer2->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(Panel1, ID_STATICTEXT1, _("URL:"), wxDefaultPosition, wxSize(47,17), 0, _T("ID_STATICTEXT1"));
	BoxSizer2->Add(StaticText2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TextCtrl1 = new wxTextCtrl(Panel1, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	BoxSizer2->Add(TextCtrl1, 2, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Button_1 = new wxButton(Panel1, ID_BUTTON2, _("GO!"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
	BoxSizer2->Add(Button_1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	BoxSizer2->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Panel1->SetSizer(BoxSizer2);
	BoxSizer2->Fit(Panel1);
	BoxSizer2->SetSizeHints(Panel1);
	BoxSizer1->Add(Panel1, 0, wxALL|wxEXPAND, 5);
	pn_web = new wxwCefPanel(this, ID_PANEL1, wxDefaultPosition, wxSize(810,307), wxTAB_TRAVERSAL, _T("ID_PANEL1"));
	BoxSizer1->Add(pn_web, 1, wxALL|wxEXPAND, 5);
	SetSizer(BoxSizer1);
	SetSizer(BoxSizer1);
	Layout();

	Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxwMainFrame::OnButton_1Click);
	//*)

}

wxwMainFrame::~wxwMainFrame()
{
	//(*Destroy(wxwMainFrame)
	//*)
}

void wxwMainFrame::afterCreated()
{
    TextCtrl1->Bind(wxEVT_SET_FOCUS, &wxwMainFrame::OnGetFocus, this);

    pn_web->embedCefBrowser();
}

void wxwMainFrame::OnGetFocus(wxFocusEvent& evt)
{
    gtk_window_present( (GtkWindow*) this->GetHandle() );
    evt.Skip();
}

/* ******************************************************************* */

void wxwMainFrame::OnButton_1Click(wxCommandEvent& event)
{
    json j;
    j = wxAtoi(TextCtrl1->GetValue());
    pn_web->sendJSON("addSL", j, &wxwMainFrame::msgback_handler1 );
}

void wxwMainFrame::msgback_handler1(wxString str)
{
    wxMessageBox("asdf", "xdd");
}


