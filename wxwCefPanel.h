#ifndef WXWCEFPANEL_H
#define WXWCEFPANEL_H

//(*Headers(wxwCefPanel)
#include <wx/panel.h>
//*)

#include <include/cef_client.h>
#include <json.hpp>

#include "wxwMainFrame.h"

using json = nlohmann::json;

class wxwCefPanel: public wxPanel
{
	public:

		wxwCefPanel(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long, const wxString &);
		virtual ~wxwCefPanel();

        void setFocusExplicitly();
		void embedCefBrowser();
		void sendJSON(CefString function_name, json &j, void (wxwMainFrame::*ptr)(wxString) = nullptr);

		CefRefPtr<CefBrowser> m_browser;
		CefRefPtr<ClientWeb> m_client_web;

		//(*Declarations(wxwCefPanel)
		//*)

	protected:

		//(*Identifiers(wxwCefPanel)
		//*)

	private:

		//(*Handlers(wxwCefPanel)
		//*)
        void OnSize(wxSizeEvent &evt);


		DECLARE_EVENT_TABLE();
};

#endif
