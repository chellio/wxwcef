#ifndef CLIENTWEB_H
#define CLIENTWEB_H

#include "ResourceHandler.h"

#include <map>
#include <string>
#include <include/cef_client.h>

using namespace std;

class wxwCefPanel;
class wxwMainFrame;

class ClientWeb : public CefClient, public CefLifeSpanHandler, public CefRequestHandler, public CefLoadHandler, public CefFocusHandler
{
    public:
        ClientWeb(wxwCefPanel *cef_panel);

        CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() {return this;}
        CefRefPtr<CefRequestHandler> GetRequestHandler() {return this;}
        CefRefPtr<CefLoadHandler> GetLoadHandler() {return this;}
        CefRefPtr<CefFocusHandler> GetFocusHandler() {return this;}

        bool OnProcessMessageReceived(CefRefPtr<CefBrowser>browser, CefProcessId source_process, CefRefPtr<CefProcessMessage>message);

        /* CefLifeSpanHandler */
        void OnAfterCreated(CefRefPtr<CefBrowser>browser);
        bool DoClose(CefRefPtr<CefBrowser>browser);
        void OnBeforeClose(CefRefPtr<CefBrowser>browser);

        /* CefRequestHandler */
        CefRefPtr<CefResourceHandler> GetResourceHandler(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame, CefRefPtr<CefRequest>request);
        CefRefPtr<CefResponseFilter> GetResourceResponseFilter(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame, CefRefPtr<CefRequest>request, CefRefPtr<CefResponse>response);

        /* CefLoadHandler */
        void OnLoadStart(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame);
        void OnLoadEnd(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame, int httpStatusCode);

        /* CefFocusHandler */
        void OnGotFocus(CefRefPtr<CefBrowser>browser);

        wxwCefPanel *m_cef_panel;

        std::map<string, void (wxwMainFrame::*)(wxString)> msgback_handlers;

    protected:

    private:

        IMPLEMENT_REFCOUNTING(ClientWeb);
};

#endif // CLIENTWEB_H
