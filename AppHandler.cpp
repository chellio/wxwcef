#include "AppHandler.h"
#include "ResourceHandler.h"
#include "JsNative.h"

#include <include/wrapper/cef_message_router.h>

AppHandler::AppHandler()
{
}

/* CefBrowserProcessHandler */

void AppHandler::OnRenderProcessThreadCreated(CefRefPtr<CefListValue>extra_info)
{
}

/* CefRenderProcessHandler */

void AppHandler::OnRenderThreadCreated(CefRefPtr<CefListValue>extra_info)
{
}

void AppHandler::OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar>registrar)
{
}

void AppHandler::OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine>command_line)
{
    if(process_type.empty())
    {
    }
}

void AppHandler::OnContextInitialized()
{
}

void AppHandler::OnContextCreated(CefRefPtr<CefBrowser>browser, CefRefPtr<CefFrame>frame, CefRefPtr<CefV8Context>context)
{

    if( browser->IsSame(m_browser) && frame->IsMain())
    {
        CefRefPtr<CefV8Value> global = context->GetGlobal();

        CefRefPtr<JsNative> js_native = new JsNative;
        CefRefPtr<CefV8Value> js_func = CefV8Value::CreateFunction("log", js_native);
        global->SetValue("log", js_func, V8_PROPERTY_ATTRIBUTE_NONE );

        // Injecting script/client.js
        CefString code = "\
            document.addEventListener('readystatechange', function(){               \
                if(document.readyState == 'interactive') {                          \
                    var general = document.createElement('script');                 \
                    general.src = 'https://client.xyz/scripts/client_.js';          \
                    document.head.appendChild(general);                             \
                }                                                                   \
            } );                                                                    \
            ";

        frame->ExecuteJavaScript(code,"inj.js", 1);

    }

}

bool AppHandler::OnProcessMessageReceived(CefRefPtr<CefBrowser>browser, CefProcessId source_process, CefRefPtr<CefProcessMessage>msg)
{

    CefRefPtr<CefListValue> msg_args = msg->GetArgumentList();
    CefV8ValueList func_args;

    for(int i = 0; i<msg_args->GetSize(); i++)
    {
        CefValueType type = msg_args->GetType(i);

        if(type==VTYPE_STRING)
        {
            func_args.push_back(CefV8Value::CreateString(msg_args->GetString(i)));
        }
        if(type==VTYPE_INT)
        {
            func_args.push_back(CefV8Value::CreateInt(msg_args->GetInt(i)));
        }
    }

    CefRefPtr<CefV8Context> main_context = browser->GetMainFrame()->GetV8Context();

    main_context->Enter();

    CefRefPtr<CefV8Value> jsfunc = main_context->GetGlobal()->GetValue( msg->GetName() );
    if(!jsfunc->IsFunction())
    {
        LOG(INFO)<<"Function not ready...";
        return false;
    }
    CefRefPtr<CefV8Value> ret = jsfunc->ExecuteFunction( NULL, func_args );


    // Response message in JSON send back to browser process

    CefRefPtr<CefProcessMessage> msg_back = CefProcessMessage::Create(msg->GetName());
    CefRefPtr<CefListValue> list_back = msg_back->GetArgumentList();

    if(ret && ret->IsString())
    {
        list_back->SetString(0, ret->GetStringValue());
        browser->SendProcessMessage(PID_BROWSER, msg_back);
    }

    main_context->Exit();

    return true;
}

void AppHandler::OnBrowserCreated(CefRefPtr<CefBrowser>browser)
{
    m_browser = browser;
}

